<?php
$recorded_temperatures = array(78,60,62,68,71,68,73,85,66,64,76,63,75,76,73,68,62,73,72,65,74,62,62,65,64,68,73,75,79,73);
$arr_sum = array_sum($recorded_temperatures);
$arr_count = count($recorded_temperatures);
$average_temperatures = $arr_sum/$arr_count;
echo "Average Temperature is : [ $average_temperatures]";
$temperature_unique = array_unique($recorded_temperatures);
$temperature_sort = sort($temperature_unique);
$lowest_temperature = implode(' ', array_slice($temperature_unique, 0, 5) );
echo "List of five lowest temperatures : [ $lowest_temperature ]";
?>
